from flask import Flask
from api_template import ApiFactory
from api_template.config import _debug
from werkzeug.serving import run_simple

app_factory: ApiFactory = ApiFactory()
app: Flask = app_factory.app

if __name__ == '__main__':
    run_simple('127.0.0.1', 5000, app, use_debugger=_debug, use_reloader=True)