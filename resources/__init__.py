import json
from flask_restful import Resource
from flask import request, Response
from flask_jwt_extended import jwt_required
from flask_jwt_extended import create_access_token

class Login(Resource):

    def get(self):
        data: dict = {'message': 'OK', 'data': create_access_token(identity='test@mail.com')}
        return Response(json.dumps(data), status=200, mimetype='application/json')

    def post(self):
        # Example login
        email = request.json.get('email')
        password = request.json.get('password')

        if email == 'test@mail.com' and password == 'test':
            data: dict = {'message': 'OK', 'data': create_access_token(identity=email)}
            return Response(json.dumps(data), status=200, mimetype='application/json')
        else:
            data: dict = {'message': 'User not found', 'data': {}}
            return Response(json.dumps(data), status=404, mimetype='application/json')

    @staticmethod
    def endpoint() -> list:
        return ['/login']

class UserInfo(Resource):

    @jwt_required()
    def post(self):
        return {'message': 'Test OK', 'data': {}}

    @staticmethod
    def endpoint() -> list:
        return ['/user/info']
